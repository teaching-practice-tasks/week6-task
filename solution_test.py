import unittest

from solution import divide_numbers


class TestSolution(unittest.TestCase):
    def test_1(self):
        try:
            divide_numbers(1, 0)
        except ValueError as v:
            assert str(v) == "Cannot divide by zero."

    def test_2(self):
        try:
            divide_numbers("1", 2)
        except TypeError as t:
            assert str(t) == "Both arguments must be numbers."

    def test_3(self):
        try:
            divide_numbers(1, [2])
        except TypeError as t:
            assert str(t) == "Both arguments must be numbers."

    def test_4(self):
        try:
            divide_numbers(-1, 2)
        except Exception as e:
            assert str(e) == "Negative numerator not allowed"

    def test_5(self):
        try:
            divide_numbers(1, -2)
        except Exception as e:
            assert str(e) == "Negative denominator not allowed"

