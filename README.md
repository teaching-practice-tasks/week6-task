### Week 6

Implement a Python function called `divide_numbers(numerator, denominator)` that takes in two arguments, a `numerator`
and a `denominator`, and returns the result of dividing the numerator by the denominator. The function should handle
the following exceptions:

- If the denominator is zero, raise a `ValueError` with the message "Cannot divide by zero."
- If the numerator or denominator is not a number (i.e. not an int or float), raise a `TypeError` with the message "Both arguments must be numbers."
- If the numerator is negative, raise an `Exception` with the message "Negative numerator not allowed"
- If the denominator is negative, raise an `Exception` with the message "Negative denominator not allowed"


ou have to implement function `divide_numbers(numerator, denominator)` in `solution.py` file.
You may create additional functions.